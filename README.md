Safeguard Pest Control have been leading providers of pest control solutions on the Sunshine Coast since 1989. Work in a variety of settings including residential, commercial, industrial, body corporate, hospitals and schools, our services are safe and ensures there is no disruption to you.

Address: 1/22 Malkana Cres, Buddina, Queensland 4575, Australia

Phone: +61 7 5477 6675

Website: https://safeguardpestcontrol.com.au
